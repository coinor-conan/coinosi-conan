from pathlib import Path

from conans import ConanFile, AutoToolsBuildEnvironment, tools


class CoinutilsConan(ConanFile):
    name = "coinosi"
    version = "0.108.6"
    license = "EPL-1.0"
    author = "Harald Held harald.held@gmail.com"
    url = "https://gitlab.com/coinor-conan/coinosi-conan"
    description = "Osi (Open Solver Interface) provides an abstract base class to a generic linear programming (LP) solver, along with derived classes for specific solvers."
    topics = ("CoinOR", "optimization", "solver interface")
    settings = "os", "compiler", "build_type", "arch"
    generators = "pkg_config"
    requires = "coinutils/2.11.4"

    def source(self):
        self.run("git clone -b releases/0.108.6 https://github.com/coin-or/Osi.git .")

    def build(self):
        autotools = AutoToolsBuildEnvironment(self)
        autotools.configure()
        autotools.make()
        autotools.install()

    def package_info(self):
        with tools.environment_append(
                {'PKG_CONFIG_PATH': [(Path(self.package_folder) / "lib" / "pkgconfig").as_posix()] + list(
                    *[self.deps_env_info[dep].PKG_CONFIG_PATH for dep in self.deps_cpp_info.deps])}):
            pkg_config = tools.PkgConfig("osi")

            # strip -l, -L
            self.cpp_info.libs = [lib[2:] for lib in pkg_config.libs_only_l]
            self.cpp_info.libdirs = [libdir[2:] for libdir in pkg_config.libs_only_L]

            self.cpp_info.cxxflags = pkg_config.cflags

            self.env_info.PKG_CONFIG_PATH.append((Path(self.package_folder) / "lib" / "pkgconfig").as_posix())
